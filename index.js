const { isContext } = require('vm');

const rust = import('./pkg/wasm_3d');
const canvas = document.getElementById('rustCanvas');
const gl = canvas.getContext('webgl', { antialias: true });

rust.then(m => {
  if (!gl) {
    alert('Fehler beim initialisieren von WebGL');
    return;
  }

  const FPS_THROTTLE = 1000.0 / 30.0; // milliseconds
  const noconceptClient = new m.NoconceptClient();
  const initialTime = Date.now();
  var lastDrawTime = -1;

  function render() {
    window.requestAnimationFrame(render);
    const currTime = Date.now();

    if (currTime >= lastDrawTime + FPS_THROTTLE) {
      lastDrawTime = currTime;

      // if user resize the browser
      if (window.innerHeight != canvas.height || window.innerWidth != canvas.width) {
        canvas.height = window.innerHeight;
        canvas.clientHeight = window.innerHeight;
        canvas.style.height = window.innerHeight;

        canvas.width = window.innerWidth;
        canvas.clientWidth = window.innerWidth;
        canvas.style.width = window.innerWidth;

        gl.viewport(0, 0, window.innerWidth, window.innerHeight);
      }

      let elapsedTime = currTime - initialTime;
      noconceptClient.update(elapsedTime, window.innerHeight, window.innerWidth);
      noconceptClient.render();
      // Rust Update call
      // Rust Render call
    }
  }

  render();
});

