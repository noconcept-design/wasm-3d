pub const FIELD_OF_VIEW: f32 = 50. * std::f32::consts::PI / 180.; // in radians
pub const GRID_SIZE: usize = 40;
pub const Z_FAR: f32 = 100.;
pub const Z_NEAR: f32 = 0.6;
pub const Z_PLANE: f32 = -2.414213; // 3d overlapping 2d (-1 / tan(pi/8) 
//pub const Z_PLANE: f32 = -2.414213 - 1.7673; // -1 / tan(pi/8)